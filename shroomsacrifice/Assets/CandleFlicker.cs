﻿using UnityEngine;
using System.Collections;

public class CandleFlicker : MonoBehaviour {

    Light candlelight;
    //Transform flame;
    //Vector3 start;
    float intensity, offset;
    public float flickering = 1f;
    void Start()
    {
        offset = Random.value;
        candlelight = GetComponent<Light>();
        intensity = candlelight.intensity;

        //flame = candlelight.transform;
        //start = flame.position;
    }
	void Update ()
    {
	    if (candlelight.enabled)
        {
            candlelight.intensity = intensity + flickering * (Random.value - 0.5f) * Mathf.Sin(Time.time / 2f + offset);
            //flame.position = start + new Vector3(Random.value, Random.value, Random.value) * 0.01f;
        }
	}
}
