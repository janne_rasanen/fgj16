﻿using UnityEngine;
using System.Collections;

public class SideAreaBlock : MonoBehaviour {

    private SideArea sideArea;
	void Start()
    {
        sideArea = GetComponentInParent<SideArea>();
    }

    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<PlayerEater>())
            sideArea.blancherInArea = true;
    }
}
