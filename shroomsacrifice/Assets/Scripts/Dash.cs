﻿using UnityEngine;
using System.Collections;

public class Dash : MonoBehaviour {

    private bool dashing = false;
    public float dashTime = 0.5f;
    public float dashUnitsPerSecond = 4f;
    private float dashTimeLeft = 0f;

    private string buttonName;
    private Movement movement;
    private LookDirection lookDirection;
    private Vector3 dashTarget;
    private CharacterController characterController;

    public GameObject dashSoundContainer;

    private AudioSource[] dashSounds;

	void Start () {
        movement = GetComponent<Movement>();
        lookDirection = GetComponent<LookDirection>();
        characterController = GetComponent<CharacterController>();
        buttonName = "Joystick" + movement.controllerNum + "Fire1";
        dashSounds = dashSoundContainer.GetComponentsInChildren<AudioSource>();
	}
	
	void Update () {
	    if (dashing)
        {
            dashTimeLeft -= Time.deltaTime;
            if (dashTimeLeft < 0)
            {
                movement.enabled = true;
                lookDirection.enabled = true;
                dashing = false;
            }
            else
            {
                characterController.SimpleMove(dashTarget);
            }
        }
        else
        {
            if (Input.GetButtonDown(buttonName))
            {
                movement.enabled = false;
                lookDirection.enabled = false;
                dashTimeLeft = dashTime;
                dashTarget = transform.position + transform.rotation * new Vector3(0, 0, 1) * 100f;
                dashTarget.Normalize();
                dashTarget *= dashUnitsPerSecond;
                dashing = true;

                dashSounds[Random.Range(0, dashSounds.Length)].Play();
            }
        }
	}
}
