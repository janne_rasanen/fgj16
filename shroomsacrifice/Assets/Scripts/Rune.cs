﻿using UnityEngine;
using System.Collections;

public class Rune : MonoBehaviour {
    AudioSource source;
     
    void Awake()
    {
        source = GetComponent<AudioSource>();
        source.playOnAwake = false;
        source.volume = 0f;
    }
    public void FadeIn()
    {
        StartCoroutine(Fade());
    }

    IEnumerator Fade()
    {
        source.Play();
        float timer = 0f;
        while(timer < 1f)
        {
            timer += Time.deltaTime * 2f;
            source.volume = timer;
            yield return null;
        }
    }
}
