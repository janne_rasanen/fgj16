﻿using UnityEngine;
using System.Collections;

public class LookDirection : MonoBehaviour {
    private Movement movement;
    private Vector3 targetLookDirection;

    public float maxDegreesPerSecond = 10f;

	void Start () {
        movement = GetComponent<Movement>();
        targetLookDirection = new Vector3(1, 0, 0);
    }
	
	void Update () {
        if (movement.lastInput.magnitude > 0.5f)
            targetLookDirection = movement.lastInput * 100f;

        transform.rotation =
            Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetLookDirection), Time.deltaTime * maxDegreesPerSecond);
            ;
    }
}
