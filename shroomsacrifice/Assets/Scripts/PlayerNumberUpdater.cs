﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerNumberUpdater : MonoBehaviour {

    void Start()
    {
        UpdateNumber();
    }

	public void UpdateNumber()
    {
        GetComponent<Text>().text = GetComponentInParent<Movement>().controllerNum.ToString();
    }
}
