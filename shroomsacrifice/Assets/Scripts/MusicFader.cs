﻿using UnityEngine;
using System.Collections;

public class MusicFader : MonoBehaviour {
    public AudioSource evil, normal, beam;

    private float normalPortion;

	public void SetVolumes(float newNormal)
    {
        normalPortion = newNormal;
        SetVolumes();
    }

    private void SetVolumes()
    {
        normalPortion = Mathf.Clamp(normalPortion, 0f, 1f);
        normal.volume = normalPortion;
        evil.volume = 1f - normalPortion;
    }

    public void PlayBeamMusic()
    {
        evil.volume = 0;
        normal.volume = 0;
        if (!beam.isPlaying)
            beam.Play();
    }

    public void ResumeNormalMusic()
    {
        beam.Stop();
        SetVolumes();
    }

    public void StopAllMusic()
    {
        evil.volume = 0;
        normal.volume = 0;
        beam.volume = 0;
    }
}
