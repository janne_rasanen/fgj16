﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

    private static int[] scores;
	void Start () {
        if (scores == null)
        {
            scores = new int[4];
            for (int i = 0; i < scores.Length; i++)
                scores[i] = 0;
        }
	}
	
	public int GetScore(int controllerNum)
    {
        return scores[controllerNum - 1];
    }

    public void AddScore(int controllerNum)
    {
        scores[controllerNum - 1]++;
    }
}
