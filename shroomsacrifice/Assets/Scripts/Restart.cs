﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {

	void Update () {
	    if (Input.GetButtonDown("Joystick1Fire1") ||
            Input.GetButtonDown("Joystick2Fire1") ||
            Input.GetButtonDown("Joystick3Fire1") ||
            Input.GetButtonDown("Joystick4Fire1")
            )
        {
            SceneManager.LoadScene(0);
        }
	}
}
