﻿using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour {
    public Transform centerOfLevel;
    public Transform respawnArea;
    public Transform blancher;
    public float maxSpawnDistance = 12f;
    public float minSpawnDistanceFromBlancher = 11f;
    public float respawnTime = 2f;
    public ParticleSystem respawnParticles;

    private float respawnTimer = -1f;
	
	void Update () {
	    if (respawnTimer >= 0)
        {
            respawnTimer += Time.deltaTime;
            if (respawnTimer >= respawnTime)
            {
                respawnTimer = -1;
                DoRespawn();
            }
        }
	}

    public void StartRespawnCounter()
    {
        respawnTimer = 0;
        Vector3 tmp = respawnArea.position;
        tmp.y = transform.position.y;
        transform.position = tmp;

        GetComponent<Movement>().enabled = false;
        GetComponent<LookDirection>().enabled = false;
    }

    private void DoRespawn()
    {
        Vector3 newPosition;
        while (true)
        {
            newPosition = centerOfLevel.position + Random.onUnitSphere * maxSpawnDistance;
            newPosition.y = transform.position.y;
            if ((newPosition - blancher.position).magnitude > minSpawnDistanceFromBlancher)
            {
                break;
            }
        }

        transform.position = newPosition;
        GetComponent<Movement>().enabled = true;
        GetComponent<LookDirection>().enabled = true;
        respawnParticles.Play();
    }
}
