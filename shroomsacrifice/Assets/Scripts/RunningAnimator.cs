﻿using UnityEngine;
using System.Collections;

public class RunningAnimator : MonoBehaviour {

    private float runningLimit = 0.5f;
    private bool running = false;
    private Movement movement;
    private Animator animator;
	
	void Start () {
        movement = GetComponentInParent<Movement>();
        animator = GetComponent<Animator>();
	}
	
	void Update () {
	    if (movement.lastInput.magnitude > runningLimit)
        {
            if (!running)
            {
                animator.SetTrigger("Run");
                running = true;
            }
        }
        else
        {
            if (running)
            {
                animator.SetTrigger("Stand");
                running = false;
            }
        }
	}
}
