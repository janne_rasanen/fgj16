﻿using UnityEngine;
using System.Collections;

public class Referee : MonoBehaviour {

    public int eatingNeededForWinning = 3;

    public GameObject blancherWonUI, shroomsWonUI;
    public Light[] candles;
    public Color activeColor;
    private int mushroomsEaten = 0;
    private MusicFader musicFader;

    void Start()
    {
        musicFader = FindObjectOfType<MusicFader>();
        UpdateMusicVolume();
    }

	void Update () {
	    if (mushroomsEaten >= eatingNeededForWinning && !blancherWonUI.activeSelf)
        {
            blancherWonUI.SetActive(true);
            musicFader.StopAllMusic();
            mushroomsEaten = 0;
        }
	}

    public void MushroomEaten()
    {
        Light tmp = candles[Mathf.Clamp(mushroomsEaten, 0, candles.Length - 1)];
        tmp.enabled = true;
        tmp.color = activeColor;
        
        tmp.transform.parent.GetChild(2).gameObject.SetActive(true);

        mushroomsEaten++;
        UpdateMusicVolume();
    }

    private void UpdateMusicVolume()
    {
        float relativeState = 1f - (float)mushroomsEaten / (float)eatingNeededForWinning;
        musicFader.SetVolumes(relativeState);
    }
}
