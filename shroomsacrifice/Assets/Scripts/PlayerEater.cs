﻿using UnityEngine;
using System.Collections;

public class PlayerEater : MonoBehaviour {
    public GameObject deathSoundContainer;
    

    private AudioSource[] deathSounds;

    private Referee referee;
    private HippuPowerController hippuController;

	void Start () {
        deathSounds = deathSoundContainer.GetComponentsInChildren<AudioSource>();
        hippuController = FindObjectOfType<HippuPowerController>();
        referee = FindObjectOfType<Referee>();
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetInstanceID() == gameObject.GetInstanceID())
        {
            return;
        }

        if (!other.gameObject.GetComponent<Movement>())
        {
            return;
        }

        int soundIndex = Random.Range(0, deathSounds.Length);
        deathSounds[soundIndex].Play();

        referee.MushroomEaten();
        hippuController.shroomEatenDuringBeam = true;

        other.GetComponent<Respawn>().StartRespawnCounter();
    }
}
