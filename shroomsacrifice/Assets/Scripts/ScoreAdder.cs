﻿using UnityEngine;
using System.Collections;

public class ScoreAdder : MonoBehaviour {
    public bool forBlancher; // if not blancher, then shrooms
    public GameObject scoreCard;

	void Start () {
        int blancherNum = 0;

        foreach(var m in FindObjectsOfType<Movement>())
        {
            if (m.GetComponent<PlayerEater>())
            {
                blancherNum = m.controllerNum;
                break;
            }
        }

        ScoreManager manager = FindObjectOfType<ScoreManager>();
        for(int i=1; i < 5; i++)
        {
            if (i == blancherNum && forBlancher ||
                i != blancherNum && !forBlancher)
            {
                Debug.Log("Adding score for " + i);
                manager.AddScore(i);
            }
        }

        scoreCard.SetActive(true);
	}
}
