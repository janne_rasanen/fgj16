﻿using UnityEngine;
using System.Collections;

public class SideArea : MonoBehaviour {

    public bool blancherInArea = false;

    private BlancherDamageChecker damageChecker;

	void Start () {
        damageChecker = FindObjectOfType<BlancherDamageChecker>();
	}
	
	void Update () {
        damageChecker.blancherInForbiddenZone = blancherInArea;
        blancherInArea = false;
	}
}
