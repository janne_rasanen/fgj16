﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
    public float speed = 1f; // units per second
    [Range(1, 4)]
    public int controllerNum = 1;

    private CharacterController characterController;
    private Vector3 diff;

    [HideInInspector]
    public Vector3 lastInput;
    string horizontal, vertical;

	void Start () {
        characterController = GetComponent<CharacterController>();
        horizontal = "Horizontal" + controllerNum;
        vertical = "Vertical" + controllerNum;
    }
	
	void Update () {
        lastInput.Set(Input.GetAxis(horizontal), 0, -Input.GetAxis(vertical));
        diff = lastInput;
        
        diff *= speed;
        // diff *= Time.deltaTime;

        characterController.SimpleMove(diff);
	}
}
