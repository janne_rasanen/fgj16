﻿using UnityEngine;
using System.Collections;

public class DisableMovement : MonoBehaviour {
    void Start()
    {
        Activate(false);
    }
	
	public void Activate(bool status) {
	    foreach(var c in FindObjectsOfType<Movement>())
        {
            c.enabled = status;
        }
        foreach (var c in FindObjectsOfType<Dash>())
        {
            c.enabled = status;
        }
        foreach (var c in FindObjectsOfType<LookDirection>())
        {
            c.enabled = status;
        }
        FindObjectOfType<HippuGenerator>().enabled = status;
    }
}
