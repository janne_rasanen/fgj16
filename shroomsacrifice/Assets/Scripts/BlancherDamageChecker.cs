﻿using UnityEngine;
using System.Collections;

public class BlancherDamageChecker : MonoBehaviour
{
    Transform[] shrooms;
    public ParticleSystem damageParticles;
    public GameObject shroomWinGameObject;
    public AudioSource painAudio;

    public float trappedDurationNeededForWin = 3f;

    private float blancherTrappedDuration = 0f;
    private MusicFader musicFader;

    public bool shroomPowerConnecting;

    [HideInInspector]
    public bool blancherInForbiddenZone;
    public float forbiddenZoneTimeModifier = 0.25f;

    void Start()
    {
        musicFader = FindObjectOfType<MusicFader>();
    }

    public void SetShrooms(Transform shroom1, Transform shroom2, Transform shroom3)
    {
        shrooms = new Transform[3];
        shrooms[0] = shroom1;
        shrooms[1] = shroom2;
        shrooms[2] = shroom3;
    }

    public void ShroomPowerActivated()
    {
        if (checkPositionLoop == null)
            checkPositionLoop = StartCoroutine(CheckingPosition());
    }

    public void ShroomPowerDeactivated()
    {
        if (checkPositionLoop != null)
        {
            StopCoroutine(checkPositionLoop);
            checkPositionLoop = null;
        }
        blancherTrappedDuration = 0f;
    }

    Coroutine checkPositionLoop;

    IEnumerator CheckingPosition()
    {
        while (true)
        {
            if (shroomPowerConnecting)
            {
                if (PointInTriangle(transform.position, shrooms[0].position, shrooms[1].position, shrooms[2].position))
                {
                    StartEffects();

                    blancherTrappedDuration += Time.deltaTime;
                }
                else if (blancherInForbiddenZone)
                {
                    blancherTrappedDuration += Time.deltaTime * forbiddenZoneTimeModifier;
                }
                else
                {
                    StopEffects();
                }

                if (blancherTrappedDuration >= trappedDurationNeededForWin)
                {
                    shroomWinGameObject.SetActive(true);
                    musicFader.StopAllMusic();
                }
            }
            else
            {
                StopEffects();

                
            }
            yield return null;
        }
    }

    private void StartEffects()
    {
        if (!damageParticles.isPlaying)
            damageParticles.Play();

        if (!painAudio.isPlaying)
            painAudio.Play();
    }

    private void StopEffects()
    {
        if (damageParticles.isPlaying)
            damageParticles.Stop();

        if (painAudio.isPlaying)
            painAudio.Stop();
    }


    float sign(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        return (p1.x - p3.x) * (p2.z - p3.z) - (p2.x - p3.x) * (p1.z - p3.z);
    }

    bool PointInTriangle(Vector3 pt, Vector3 v1, Vector3 v2, Vector3 v3)
    {
        bool b1, b2, b3;

        b1 = sign(pt, v1, v2) < 0.0f;
        b2 = sign(pt, v2, v3) < 0.0f;
        b3 = sign(pt, v3, v1) < 0.0f;

        return ((b1 == b2) && (b2 == b3));
    }

    public float GetDamageRelativeState()
    {
        return Mathf.Clamp(blancherTrappedDuration / trappedDurationNeededForWin, 0f, 1f);
    }
}
