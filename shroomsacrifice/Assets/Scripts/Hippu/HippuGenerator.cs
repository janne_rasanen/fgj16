﻿using UnityEngine;
using System.Collections;

public class HippuGenerator : MonoBehaviour {
    public GameObject hippuPrefab;
    public float spawnTimer;
    public float timeToSpawn;

    void Update ()
    {
        if (spawnTimer > timeToSpawn)
        {
            SpawnHippu();
            spawnTimer = 0f;
        }
        spawnTimer += Time.deltaTime;
	}

    void SpawnHippu()
    {
        GameObject hippu = Instantiate(hippuPrefab) as GameObject;
        hippu.transform.position = new Vector3(Random.Range(-10, 11), 2f, Random.Range(-10, 11));
    }
}
