﻿using UnityEngine;
using System.Collections;

public class ShroomPower : MonoBehaviour {
    public LineRenderer blockedLine1;
    public LineRenderer blockedLine2;
    public LineRenderer connectedLine;
    public LineRenderer statusIndicatorLine;

    Vector3 target1, target2, target3 = Vector3.zero;
    public GameObject particlePrefab;

    public Material connectedMaterial, tooFarMaterial;

    GameObject particles1, particles2;
    bool powerActive = false;

    private BlancherDamageChecker damageChecker;

	void Start ()
    {
        particles1 = Instantiate(particlePrefab) as GameObject;
        particles1.transform.parent = blockedLine1.transform;
        particles2 = Instantiate(particlePrefab) as GameObject;
        particles2.transform.parent = blockedLine2.transform;
        damageChecker = FindObjectOfType<BlancherDamageChecker>();
        SetPowerActive(false);
    }

    void Update()
    {
        if (powerActive)
        {
            blockedLine1.SetPosition(0, transform.position);
            blockedLine2.SetPosition(0, transform.position);
            connectedLine.SetPosition(0, transform.position);
            statusIndicatorLine.SetPosition(0, transform.position);
        }
    }

    public void LineBlocked(int lineNumber, Vector3 target)
    {
        if (lineNumber == 1)
        {
            connectedLine.enabled = false;
            statusIndicatorLine.enabled = false;
            blockedLine1.gameObject.SetActive(true);
            target1 = target;
            //blockedLine1.SetPosition(1, target);
            particles1.SetActive(true);
            particles1.transform.position = target;
        }
        else
        {
            blockedLine2.gameObject.SetActive(true);
            target2 = target;
            //blockedLine2.SetPosition(1, target);
            particles2.SetActive(true);
            particles2.transform.position = target;
        }
    }

    public void PowerConnecting(int lineNumber, Vector3 target)
    {
        if (lineNumber == 1)
        {
            blockedLine1.gameObject.SetActive(false);
            //particles1.SetActive(false);
            connectedLine.enabled = true;
            statusIndicatorLine.enabled = true;
            //connectedLine.SetPosition(1, target);
            target3 = target;
            connectedLine.material = connectedMaterial;
        }
        else
        {
            blockedLine2.gameObject.SetActive(false);
            //particles2.SetActive(false);
        }
    }

    public void TooFar(int lineNumber, Vector3 target)
    {
        if (lineNumber == 1)
        {
            blockedLine1.gameObject.SetActive(false);
            connectedLine.enabled = true;
            //connectedLine.SetPosition(1, target);
            target3 = target;
            connectedLine.material = tooFarMaterial;
        }
        else
        {
            blockedLine2.gameObject.SetActive(false);
        }
    }

    void LateUpdate()
    {
        blockedLine1.SetPosition(1, target1);
        blockedLine2.SetPosition(1, target2);
        connectedLine.SetPosition(1, target3);

        Vector3 diff = target3 - transform.position;
        diff *= damageChecker.GetDamageRelativeState();
        statusIndicatorLine.SetPosition(1, transform.position + diff);
    }

    public void SetPowerActive(bool active)
    {
        powerActive = active;
        blockedLine1.enabled = active;
        blockedLine2.enabled = active;
        blockedLine1.gameObject.SetActive(active);
        blockedLine2.gameObject.SetActive(active);
        connectedLine.gameObject.SetActive(active);
        statusIndicatorLine.gameObject.SetActive(active);
    }
}
