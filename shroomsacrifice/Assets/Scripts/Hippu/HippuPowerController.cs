﻿using UnityEngine;
using System.Collections;

public class HippuPowerController : MonoBehaviour
{
    const float MAX_POWER = 5f;
    public float currentPower = 0f;
    public float powerActiveTime;
    public float maxDistance;
    public GameObject[] runes;

    static HippuPowerController instance;
    public static HippuPowerController Instance { get { return instance; } }

    public ShroomPower[] shroomPlayers = new ShroomPower[3];
    public BlancherDamageChecker blancherDamageCheck;
    public bool shroomEatenDuringBeam;
    public GameObject holyGroundEffectContainer;

    private AudioSource beamSound;
    private MusicFader musicFader;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }

        for (int i = 0; i < runes.Length; i++)
        {
            runes[i].SetActive(false);
        }

        instance = this;
        blancherDamageCheck.SetShrooms(shroomPlayers[0].transform, shroomPlayers[1].transform, shroomPlayers[2].transform);
        beamSound = GetComponent<AudioSource>();
        musicFader = FindObjectOfType<MusicFader>();
    }

    void OnDestroy()
    {
        instance = null;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            HippuGathered();
    }

    public void HippuGathered()
    {
        GameObject go = runes[Mathf.Clamp((int)currentPower, 0, runes.Length - 1)];
        go.SetActive(true);
        go.GetComponent<Rune>().FadeIn();
        currentPower++;
        if (currentPower >= MAX_POWER)
        {
            ActivateHippuPower();
        }
    }

    Coroutine powerLoop;
    void ActivateHippuPower()
    {
        if (powerLoop == null)
        {
            shroomEatenDuringBeam = false;
            powerLoop = StartCoroutine(UpdatePower());   
        }   
    }

    IEnumerator UpdatePower()
    {
        blancherDamageCheck.ShroomPowerActivated();
        for (int i = 0; i < shroomPlayers.Length; i++)
        {
            shroomPlayers[i].SetPowerActive(true);
        }

        if (!beamSound.isPlaying)
            beamSound.Play();

        musicFader.PlayBeamMusic();
        holyGroundEffectContainer.SetActive(true);

        for (int i = 0; i < runes.Length; i++)
        {
            runes[i].GetComponent<AudioSource>().Stop();
        }

        while (currentPower > 0f)
        {
            currentPower -= MAX_POWER * Time.deltaTime / powerActiveTime;

            bool eka = CheckObstaclesBetween(shroomPlayers[0], shroomPlayers[1]);
            bool toka = CheckObstaclesBetween(shroomPlayers[1], shroomPlayers[2]);
            bool kolmas = CheckObstaclesBetween(shroomPlayers[2], shroomPlayers[0]);
            blancherDamageCheck.shroomPowerConnecting = eka && toka && kolmas;

            if (shroomEatenDuringBeam)
            {
                shroomEatenDuringBeam = false;
                currentPower = 0f;
            }

            yield return null;
        }
        powerLoop = null;
        beamSound.Stop();
        musicFader.ResumeNormalMusic();
        holyGroundEffectContainer.SetActive(false);

        blancherDamageCheck.ShroomPowerDeactivated();
        for (int i = 0; i < shroomPlayers.Length; i++)
        {
            shroomPlayers[i].SetPowerActive(false);
        }
        for (int i = 0; i < runes.Length; i++)
        {
            runes[i].SetActive(false);
        }
    }

    bool CheckObstaclesBetween(ShroomPower shroom1, ShroomPower shroom2)
    {
        RaycastHit hit;
        if (Vector3.Distance(shroom1.transform.position, shroom2.transform.position) > maxDistance)
        {
            shroom1.TooFar(1, shroom2.transform.position);
            shroom2.TooFar(2, shroom1.transform.position);

            return false;
        }

        if (Physics.Raycast(shroom1.transform.position,
            shroom2.transform.position - shroom1.transform.position,
            out hit,
            Vector3.Distance(shroom1.transform.position, shroom2.transform.position) *2f, ~(1 << 10)))
        {
            /*
            if (hit.transform.tag != "Shroom")
            {
                shroom1.LineBlocked(1, hit.point);

                if (Physics.Raycast(shroom2.transform.position,
                    shroom1.transform.position - shroom2.transform.position, 
                    out hit,
                    Vector3.Distance(shroom1.transform.position, shroom2.transform.position) *2f, ~(1<<10)))
                {
                    shroom2.LineBlocked(2, hit.point);
                }
                return false;
            }
            else
            */
            {
                shroom1.PowerConnecting(1, shroom2.transform.position);
                shroom2.PowerConnecting(2, shroom1.transform.position);
                return true;
            }
        }

        return false;
    }
}
