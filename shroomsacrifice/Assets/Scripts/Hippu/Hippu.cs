﻿using UnityEngine;
using System.Collections;

public class Hippu : MonoBehaviour
{
    Vector3 startPos;
    float startTime;
    [SerializeField] Transform graphics;
    [SerializeField]
    AudioClip[] clips;
    void Start()
    {
        startTime = Time.time;
        startPos = graphics.position;
    }

    void Update()
    {
        graphics.position = startPos + 0.75f* Vector3.up * Mathf.Sin(Time.time + startTime);
    }

    void OnTriggerEnter(Collider other)
    {
        HippuPowerController.Instance.HippuGathered();
        AudioSource aud = GetComponent<AudioSource>();
        aud.clip = clips[Random.Range(0, clips.Length)];
        aud.Play();
        GetComponent<CapsuleCollider>().enabled = false;
        graphics.gameObject.SetActive(false);
        Destroy(gameObject, 2);
    }
}
