﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreShower : MonoBehaviour {

    private Text text;

	void Start () {
        string result = "";
        ScoreManager scores = FindObjectOfType<ScoreManager>();
        for(int i=1; i < 5; i++)
        {
            result += scores.GetScore(i);
            if (i != 4)
                result += '\n';
        }
        GetComponent<Text>().text = result;
	}
}
