﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameStartCooldown : MonoBehaviour {

    public float secondsToStart = 3f;

    private float secondsLeft;

    public GameObject[] numberCanvases;
    public bool randomizeControllers = false;

	void Awake () {
        secondsLeft = secondsToStart;
        if (randomizeControllers)
            RandomizeControllers();
	}

    private void RandomizeControllers()
    {
        List<int> available = new List<int>();
        available.AddRange(new int[] { 1, 2, 3, 4 });

        foreach(var canvas in numberCanvases)
        {
            Movement m = canvas.GetComponentInParent<Movement>();
            int randomIndex = Random.Range(0, available.Count);
            m.controllerNum = available[randomIndex];
            available.RemoveAt(randomIndex);
        }
    }

    void Update () {
        secondsLeft -= Time.deltaTime;
        if (secondsLeft < 0)
        {
            StartGame();
            this.enabled = false;
        }
	}

    private void StartGame()
    {
        foreach(var canvas in numberCanvases)
        {
            canvas.SetActive(false);
        }
        GetComponent<DisableMovement>().Activate(true);
    }
}
